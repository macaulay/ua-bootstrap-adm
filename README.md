# UA Bootstrap #

This repository is the UA's flavor of bootstrap, combining the new UA Brand with the goodness of Bootstrap 3.

## Versioning Scheme ##

Because this front end CSS framework is based on Bootstrap 3, tagged versions use a modified form of [SemVer](http://semver.org/).
All versioned releases will be tagged with a 4 point version.
The first number is the major version of bootstrap being used (3).
The following 3 numbers will be using the normal SemVer scheme (major.minor.patch).

## How to view everything

### Dependencies

npm
gulp
bower

### In the command line run:

Navigate to the folder where you keep your clone of ua-bootstrap or...
`git clone git@bitbucket.org:uadigital/ua-bootstrap.git `

```
git fetch && git checkout master
npm install
gulp
gulp serve
```

[Then go here in your browser](http://localhost:8888)

## On making edits to UA-Bootstrap

UA-Bootstrap is entirely based of Twitter's Bootstrap and thus is largely
a bunch of overrides. But Twitter Bootstrap doesn't have eveything we'd like in
UA-Boostrap, so when coming across a change that is not an override to Vanilla
Twitter Bootstrap, place those additions/appendages in `_custom.scss`.

## Requirements:

1. Install [NodeJS](https://nodejs.org/download/)
   & [Git](https://git-scm.com/downloads)
2. Create [BitBucket](https://bitbucket.org) account
3. Fork this version of
   [UA-Bootstrap](https://bitbucket.org/uadigital/ua-bootstrap/fork)
4. Clone the forked repo from your bitbucket account `git clone
   git@bitbucket.org:<USERNAME>/ua-bootstrap.git`
5. Open a terminal and change directory "cd" to where you've downloaded the forked repo
6. Run this command in your terminal `npm install gulp -g && npm install &&
   gulp`
7. Install the
   [LiveReload](https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei)
   for [Chrome](http://www.google.com/chrome).
8. Open [http://localhost:8888](http://localhost:8888) in
   [Chrome](http://www.google.com/chrome), and voilà.


## Making edits in Chrome persistent

Picking up from the last step, you should now have Twitter's Bootstrap documentation running
locally. Now to wire up Chrome DevTools to write changes out to the files you
are serving up. Go back to the browser and fire up Chrome DevTools and head over
to the __Sources__ tab. There should be a __Sources__ pane with a list of
domains where all your assets are being downloaded from. Right click in that pane
and select __Add Folder to Workspace__.

![Add Folder to
Workspace](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-1.png)

Add the directory where the ua-bootstrap repo is located. For me it was on my
Desktop so I added `~/Desktop/ua-bootstrap`. Now the folder is added to your
workspace. Now to link the files to the folder in your workspace. Go back to the
__Sources__ pane and right-click `ua-bootstrap.scss` in the folder titled
`source` under `localhost:8888`. Click __Map to File System Resource...__ and it
should auto-find the file in the added workspace folder. Hit enter and you're
set.

![Map to File System Resource...](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-2.png)

![ua-bootstrap.scss](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-3.png)

One caveat on using Chrome inspector to edit. Chrome does not have the ability
to make edits to the `*.scss` under the __Elements__ tab. You'll have to make
all edits in the __Sources__ tab, which is a bit of a pain. So find the files
you need to override in the __Elements__ tab, but then select the reference link
to bring you to the __Sources__ tab where you can make the edit and save.

![Make edit in Sources Tab](https://bitbucket.org/uadigital/ua-bootstrap/raw/HEAD/docs/img/readme-4.png)