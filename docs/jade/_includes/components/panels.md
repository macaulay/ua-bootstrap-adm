<h1 id="panels">Panels</h1>

<p class="lead">While not always necessary, sometimes you need to put your DOM in a box. For those situations, try the panel component.</p>

<h3 id="panels-basic">Basic example</h3>
<p>By default, all the <code>.panel</code> does is apply some basic border and padding to contain some content.</p>
<div class="example" data-example-id="simple-panel">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-default">
    <div class="panel-body">
      Basic panel example
    </div>
  </div>
</div>

```html
<div class="panel panel-default">
<div class="panel-body">
  Basic panel example
</div>
</div>
```

<h3 id="panels-heading">Panel with heading</h3>
<p>Easily add a heading container to your panel with <code>.panel-heading</code>. You may also include any <code>&lt;h1&gt;</code>-<code>&lt;h6&gt;</code> with a <code>.panel-title</code> class to add a pre-styled heading.</p>
<p>For proper link coloring, be sure to place links in headings within <code>.panel-title</code>.</p>
<div class="example" data-example-id="panel-with-heading">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-default">
    <div class="panel-heading">Panel heading without title</div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
</div>

```html
<div class="panel panel-default">
<div class="panel-heading">Panel heading without title</div>
<div class="panel-body">
  Panel content
</div>
</div>

<div class="panel panel-default">
<div class="panel-heading">
  <h3 class="panel-title">Panel title</h3>
</div>
<div class="panel-body">
  Panel content
</div>
</div>
```

<h3 id="panels-footer">Panel with footer</h3>
<p>Wrap buttons or secondary text in <code>.panel-footer</code>. Note that panel footers <strong>do not</strong> inherit colors and borders when using contextual variations as they are not meant to be in the foreground.</p>
<div class="example" data-example-id="panel-with-footer">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-default">
    <div class="panel-body">
      Panel content
    </div>
    <div class="panel-footer">Panel footer</div>
  </div>
</div>

```html
<div class="panel panel-default">
<div class="panel-body">
  Panel content
</div>
<div class="panel-footer">Panel footer</div>
</div>
```

<h3 id="panels-alternatives">Contextual alternatives</h3>
<p>Like other components, easily make a panel more meaningful to a particular context by adding any of the contextual state classes.</p>
<div class="example" data-example-id="contextual-panels">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
  <div class="panel panel-info">
    <div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
  <div class="panel panel-warning">
    <div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
  <div class="panel panel-danger">
    <div class="panel-heading">
      <h3 class="panel-title">Panel title</h3>
    </div>
    <div class="panel-body">
      Panel content
    </div>
  </div>
</div>

```html
<div class="panel panel-primary">...</div>
<div class="panel panel-success">...</div>
<div class="panel panel-info">...</div>
<div class="panel panel-warning">...</div>
<div class="panel panel-danger">...</div>
```

<h3 id="panels-tables">With tables</h3>
<p>Add any non-bordered <code>.table</code> within a panel for a seamless design. If there is a <code>.panel-body</code>, we add an extra border to the top of the table for separation.</p>
<div class="example" data-example-id="table-within-panel">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-default">
    <div class="panel-heading">Panel heading</div>
    <div class="panel-body">
      <p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
    </div>
    <div class="table-responsive">    
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>University of Arizona</td>
            <td>arizona.edu</td>
            <td>@UofA</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Arizona Alumni</td>
            <td>arizonaalumni.com</td>
            <td>@UAAA</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Arizona Athletics</td>
            <td>arizonawildcats.com</td>
            <td>@AZATHLETICS</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

```html
<div class="panel panel-default">
<!-- Default panel contents -->
<div class="panel-heading">Panel heading</div>
<div class="panel-body">
  <p>...</p>
</div>
<!-- Table -->
<div class="table-responsive">
  <table class="table">
    ...
  </table>
</div>  
</div>
```

<p>If there is no panel body, the component moves from panel header to table without interruption.</p>
<div class="example" data-example-id="panel-without-body-with-table">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-default">
    <div class="panel-heading">Panel heading</div>
    <div class="table-responsive">
      <table class="table">
        <thead>
          <tr>
            <th>#</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Username</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th scope="row">1</th>
            <td>University of Arizona</td>
            <td>arizona.edu</td>
            <td>@UofA</td>
          </tr>
          <tr>
            <th scope="row">2</th>
            <td>Arizona Alumni</td>
            <td>arizonaalumni.com</td>
            <td>@UAAA</td>
          </tr>
          <tr>
            <th scope="row">3</th>
            <td>Arizona Athletics</td>
            <td>arizonawildcats.com</td>
            <td>@AZATHLETICS</td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

```html
<div class="panel panel-default">
<!-- Default panel contents -->
<div class="panel-heading">Panel heading</div>

<!-- Table -->
<div class="table-responsive">
  <table class="table">
    ...
  </table>
</div>  
</div>
```


<h3 id="panels-list-group">With list groups</h3>
<p>Easily include full-width <a href="#list-group">list groups</a> within any panel.</p>
<div class="example" data-example-id="panel-with-list-group">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="panel panel-default">
    <div class="panel-heading">Panel heading</div>
    <div class="panel-body">
      <p>Some default panel content here. Nulla vitae elit libero, a pharetra augue. Aenean lacinia bibendum nulla sed consectetur. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
    </div>
    <ul class="list-group">
      <li class="list-group-item">Cras justo odio</li>
      <li class="list-group-item">Dapibus ac facilisis in</li>
      <li class="list-group-item">Morbi leo risus</li>
      <li class="list-group-item">Porta ac consectetur ac</li>
      <li class="list-group-item">Vestibulum at eros</li>
    </ul>
  </div>
</div>

```html
<div class="panel panel-default">
<!-- Default panel contents -->
<div class="panel-heading">Panel heading</div>
<div class="panel-body">
  <p>...</p>
</div>

<!-- List group -->
<ul class="list-group">
  <li class="list-group-item">Cras justo odio</li>
  <li class="list-group-item">Dapibus ac facilisis in</li>
  <li class="list-group-item">Morbi leo risus</li>
  <li class="list-group-item">Porta ac consectetur ac</li>
  <li class="list-group-item">Vestibulum at eros</li>
</ul>
</div>
```
