<h1 id="buttons">Buttons</h1>

<h2 id="buttons-tags">Button tags</h2>
  Use the button classes on an `<a>`, `<button>`, or `<input>` element.

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <form class="bs-example" data-example-id="btn-tags">
    <a class="btn btn-default" href="#" role="button">Link</a>
    <button class="btn btn-default" type="submit">Button</button>
    <input class="btn btn-default" type="button" value="Input">
    <input class="btn btn-default" type="submit" value="Submit">
  </form>
</div>

```html
 <a class="btn btn-default" href="#" role="button">Link</a>
 <button class="btn btn-default" type="submit">Button</button>
 <input class="btn btn-default" type="button" value="Input">
 <input class="btn btn-default" type="submit" value="Submit">
```

#### Context-specific usage
While button classes can be used on `<a>` and `<button>` elements, only `button` elements are supported within our nav and navbar components.

#### Links acting as buttons

If the `<a>` elements are used to act as buttons – triggering in-page functionality, rather than navigating to another document or section within the current page – they should also be given an appropriate `role="button"`.

#### Cross-browser rendering

As a best practice, **we highly recommend using the `<button>` element whenever possible** to ensure matching cross-browser rendering.

Among other things, there's [https://bugzilla.mozilla.org/show_bug.cgi?id=697451](a bug in Firefox &lt;30) that prevents us from setting the `line-height` of `<input`-based buttons, causing them to not exactly match the height of other buttons on Firefox.

<h2 id="buttons-options">Options</h2>
Use any of the available button classes to quickly create a styled button.
<div class="example" data-example-id="btn-variants">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <button type="button" class="btn btn-default">Default</button>
 <button type="button" class="btn btn-primary">Primary</button>
 <button type="button" class="btn btn-success">Success</button>
 <button type="button" class="btn btn-info">Info</button>
 <button type="button" class="btn btn-warning">Warning</button>
 <button type="button" class="btn btn-danger">Danger</button>
 <button type="button" class="btn btn-link">Link</button>
</div>

```html
<!-- Standard button -->
<button type="button" class="btn btn-default">Default</button>

<!-- Provides extra visual weight and identifies the primary action in a set of buttons -->
<button type="button" class="btn btn-primary">Primary</button>

<!-- Indicates a successful or positive action -->
<button type="button" class="btn btn-success">Success</button>

<!-- Contextual button for informational alert messages -->
<button type="button" class="btn btn-info">Info</button>

<!-- Indicates caution should be taken with this action -->
<button type="button" class="btn btn-warning">Warning</button>

<!-- Indicates a dangerous or potentially negative action -->
<button type="button" class="btn btn-danger">Danger</button>

<!-- Deemphasize a button by making it look like a link while maintaining button behavior -->
<button type="button" class="btn btn-link">Link</button>
```
<a id="css-buttons-arrow-button"></a>
### Arrow Buttons

You can add arrows by including the

<div class="example">
 <p class="example-label text-size-h3">
     <span class="label label-info">Example</span>
 </p>
 <a href='#' class="btn btn-arrow btn-red btn-lg">Large Button</a>
 <a href='#' class="btn btn-arrow btn-blue btn-lg">Large Button</a>
 <a href='#' class="btn btn-arrow btn-red">Default Button</a>
 <a href='#' class="btn btn-arrow btn-blue">Default Button</a>
 <a href='#' class="btn btn-arrow btn-red btn-sm">Small Button</a>
 <a href='#' class="btn btn-arrow btn-blue btn-sm">Small Button</a>
</div>

```html
<!-- Large buttons -->
<a href="#" class="btn-arrow btn btn-red btn-lg">Large Button</a>
<a href="#" class="btn-arrow btn btn-blue btn-lg">Large Button</a>

<!-- Default buttons -->
<a href="#" class="btn-arrow btn btn-red">Default Button</a>
<a href="#" class="btn-arrow btn btn-blue">Default Button</a>

<!-- Small buttons -->
<a href="#" class="btn-arrow btn btn-red btn-sm">Small Button</a>
<a href="#" class="btn-arrow btn btn-blue btn-sm">Small Button</a>
```

  <div class="bs-callout bs-callout-warning" id="callout-buttons-color-accessibility">
   #### Conveying meaning to assistive technologies
   Using color to add meaning to a button only provides a visual indication, which will not be conveyed to users of assistive technologies – such as screen readers. Ensure that information denoted by the color is either obvious from the content itself (the visible text of the button), or is included through alternative means, such as additional text hidden with the `.sr-only` class.
  </div>

  <h2 id="buttons-sizes">Sizes</h2>
  <p>Fancy larger or smaller buttons? Add <code>.btn-lg</code>, <code>.btn-sm</code>, or <code>.btn-xs</code> for additional sizes.</p>
  <div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
   <p>
   <button type="button" class="btn btn-primary btn-lg">Large button</button>
   <button type="button" class="btn btn-default btn-lg">Large button</button>
   </p>
   <p>
   <button type="button" class="btn btn-primary">Default button</button>
   <button type="button" class="btn btn-default">Default button</button>
   </p>
   <p>
   <button type="button" class="btn btn-primary btn-sm">Small button</button>
   <button type="button" class="btn btn-default btn-sm">Small button</button>
   </p>
   <p>
   <button type="button" class="btn btn-primary btn-xs">Extra small button</button>
   <button type="button" class="btn btn-default btn-xs">Extra small button</button>
   </p>
  </div>
```html
<p>
  <button type="button" class="btn btn-primary btn-lg">Large button</button>
  <button type="button" class="btn btn-default btn-lg">Large button</button>
</p>
<p>
  <button type="button" class="btn btn-primary">Default button</button>
  <button type="button" class="btn btn-default">Default button</button>
</p>
<p>
  <button type="button" class="btn btn-primary btn-sm">Small button</button>
  <button type="button" class="btn btn-default btn-sm">Small button</button>
</p>
<p>
  <button type="button" class="btn btn-primary btn-xs">Extra small button</button>
  <button type="button" class="btn btn-default btn-xs">Extra small button</button>
</p>
```
<div>
  <p>Create block level buttons&mdash;those that span the full width of a parent&mdash; by adding <code>.btn-block</code>.</p>
</div>  
  <div class="bs-example" data-example-id="block-btns">
    <div class="well center-block" style="max-width: 400px;">
      <button type="button" class="btn btn-primary btn-lg btn-block">Block level button</button>
      <button type="button" class="btn btn-default btn-lg btn-block">Block level button</button>
    </div>
  </div>
  
```html
<button type="button" class="btn btn-primary btn-lg btn-block">Block level button</button>
<button type="button" class="btn btn-default btn-lg btn-block">Block level button</button>
```


  <h2 id="buttons-active">Active state</h2>
  <p>Buttons will appear pressed (with a darker background, darker border, and inset shadow) when active. For <code>&lt;button&gt;</code> elements, this is done via <code>:active</code>. For <code>&lt;a&gt;</code> elements, it's done with <code>.active</code>. However, you may use <code>.active</code> on <code>&lt;button&gt;</code>s (and include the <code>aria-pressed="true"</code> attribute) should you need to replicate the active state programmatically.</p>

  <h3>Button element</h3>
  <p>No need to add <code>:active</code> as it's a pseudo-class, but if you need to force the same appearance, go ahead and add <code>.active</code>.</p>
  <p class="bs-example" data-example-id="active-button-btns">
    <button type="button" class="btn btn-primary btn-lg active">Primary button</button>
    <button type="button" class="btn btn-default btn-lg active">Button</button>
  </p>
```html
<button type="button" class="btn btn-primary btn-lg active">Primary button</button>
<button type="button" class="btn btn-default btn-lg active">Button</button>
```

  <h3>Anchor element</h3>
  <p>Add the <code>.active</code> class to <code>&lt;a&gt;</code> buttons.</p>
  <p class="bs-example" data-example-id="active-anchor-btns">
    <a href="#" class="btn btn-primary btn-lg active" role="button">Primary link</a>
    <a href="#" class="btn btn-default btn-lg active" role="button">Link</a>
  </p>
```html
<a href="#" class="btn btn-primary btn-lg active" role="button">Primary link</a>
<a href="#" class="btn btn-default btn-lg active" role="button">Link</a>
```


  <h2 id="buttons-disabled">Disabled state</h2>
  <p>Make buttons look unclickable by fading them back with <code>opacity</code>.</p>

  <h3>Button element</h3>
  <p>Add the <code>disabled</code> attribute to <code>&lt;button&gt;</code> buttons.</p>
  <p class="bs-example" data-example-id="disabled-button-btns">
    <button type="button" class="btn btn-primary btn-lg" disabled="disabled">Primary button</button>
    <button type="button" class="btn btn-default btn-lg" disabled="disabled">Button</button>
  </p>
```
<button type="button" class="btn btn-lg btn-primary" disabled="disabled">Primary button</button>
<button type="button" class="btn btn-default btn-lg" disabled="disabled">Button</button>
```

  <div class="bs-callout bs-callout-danger" id="callout-buttons-ie-disabled">
    <h4>Cross-browser compatibility</h4>
    <p>If you add the <code>disabled</code> attribute to a <code>&lt;button&gt;</code>, Internet Explorer 9 and below will render text gray with a nasty text-shadow that we cannot fix.</p>
  </div>

  <h3>Anchor element</h3>
  <p>Add the <code>.disabled</code> class to <code>&lt;a&gt;</code> buttons.</p>
  <p class="bs-example" data-example-id="disabled-anchor-btns">
    <a href="#" class="btn btn-primary btn-lg disabled" role="button">Primary link</a>
    <a href="#" class="btn btn-default btn-lg disabled" role="button">Link</a>
  </p>
```html
<a href="#" class="btn btn-primary btn-lg disabled" role="button">Primary link</a>
<a href="#" class="btn btn-default btn-lg disabled" role="button">Link</a>
```
  <p>
    We use <code>.disabled</code> as a utility class here, similar to the common <code>.active</code> class, so no prefix is required.
  </p>
  <div class="bs-callout bs-callout-warning" id="callout-buttons-disabled-anchor">
    <h4>Link functionality caveat</h4>
    <p>This class uses <code>pointer-events: none</code> to try to disable the link functionality of <code>&lt;a&gt;</code>s, but that CSS property is not yet standardized and isn't fully supported in Opera 18 and below, or in Internet Explorer 11. In addition, even in browsers that do support <code>pointer-events: none</code>, keyboard navigation remains unaffected, meaning that sighted keyboard users and users of assistive technologies will still be able to activate these links. So to be safe, use custom JavaScript to disable such links.</p>
  </div>

