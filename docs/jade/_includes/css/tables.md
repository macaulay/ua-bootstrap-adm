<a id="css-tables"></a>
## Tables


### Basic

Use the standard table code to make tables.
<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="table-responsive">
    <table class="table-striped">
      <caption>Optional table caption.</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>URL</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>University of Arizona</td>
          <td>arizona.edu</td>
          <td>@UofA</td>
        </tr>
        <tr>
          <th>2</th>
          <td>Arizona Alumni</td>
          <td>arizonaalumni.com</td>
          <td>@UAAA</td>
        </tr>
        <tr>
          <th>3</th>
          <td>Arizona Athletics</td>
          <td>arizonawildcats.com</td>
          <td>@AZATHLETICS</td>
        </tr>
      </tbody>
    </table>
  </div>  
</div>

```html
<div class="table-responsive">
  <table class="table-striped">
      ...
  </table>
</div>  
```

<a id="css-tables-hover"></a>
### Hover Rows

Add the `.table-hover` class to table elements for a blue hover effect on table rows.


<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="table-responsive">
    <table class="table-hover table-striped">
      <caption>Optional table caption.</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>URL</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>University of Arizona</td>
          <td>arizona.edu</td>
          <td>@UofA</td>
        </tr>
        <tr>
          <th>2</th>
          <td>Arizona Alumni</td>
          <td>arizonaalumni.com</td>
          <td>@UAAA</td>
        </tr>
        <tr>
          <th>3</th>
          <td>Arizona Athletics</td>
          <td>arizonawildcats.com</td>
          <td>@AZATHLETICS</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

```html
<div class="table-responsive">
  <table class="table-hover table-striped">
      ...
  </table>
</div>  
```

<a id="css-tables-responsive"></a>
### Responsive Tables

Create responsive tables by wrapping any `<table>` in `.table-responsive` to
make them scroll horizontally on small devices (under 48em). When viewing on
anything larger than 48em wide, you will not see any difference in these
tables.

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <div class="table-responsive">
    <table class="table-striped">
      <caption>Optional table caption.</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>Name</th>
          <th>URL</th>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>1</th>
          <td>University of Arizona</td>
          <td>arizona.edu</td>
          <td>@UofA</td>
        </tr>
        <tr>
          <th>2</th>
          <td>Arizona Alumni</td>
          <td>arizonaalumni.com</td>
          <td>@UAAA</td>
        </tr>
        <tr>
          <th>3</th>
          <td>Arizona Athletics</td>
          <td>arizonawildcats.com</td>
          <td>@AZATHLETICS</td>
        </tr>
      </tbody>
    </table>
  </div>
</div>

```html
<div class="table-responsive">
  <table class="table-striped">
      ...
  </table>
</div>  
```
