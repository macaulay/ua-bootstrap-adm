<a id="css-typography"></a>
## Typography

Visit the [fonts section]('/fonts.html') for tips and best practices.

### Headings

All HTML headings, `h1` through `h6`, are available. `.h1` through `.h6` classes
are also available, for when you want to match the font styling of a heading but
still want your text to be displayed inline.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <h1>Heading one example</h1>
 <hr>
 <h1 class="text-uppercase">Uppercase example</h1>
 <hr>
 <h2>Heading two example</h2>
 <hr>
 <h3>Heading three example</h3>
 <hr>
 <h4>Heading four example</h4>
 <hr>
 <h5>Heading five example</h5>
 <hr>
 <h6>Heading six example</h6>
</div>

<a id="css-typography-body-copy"></a>

### Margin Vertical Align

Default vertical alignment for headings, `h1` through `h6`, might not be set to what you need for your documents.  
You can change this by using `.margin-align-*`.

Alternately, you can use `.margin-zero-*` to override top or bottom margin to zero.

**Note!** The best results occur in block-level elements, and not inline-elements. 

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <p class="text-size-h3 margin-align-top">Text align top example</p>
  <hr>
  <p class="text-size-h3 margin-align-middle">Text align middle example</p>
  <hr>
  <p class="text-size-h3 margin-align-bottom">Text align bottom example</p>
  <hr>
  <p class="text-size-h3 margin-zero-top">Text align with zero margin-top</p>
  <hr>
  <p class="text-size-h3 margin-zero-bottom">Text align with zero margin-bottom</p>
</div>

```html
<p class="margin-align-top">Text align top example</p>
<p class="margin-align-middle">Text align middle example</p>
<p class="margin-align-bottom">Text align bottom example</p>
<p class="margin-zero-top">Text align with zero margin-top</p>
<p class="margin-zero-bottom">Text align with zero margin-bottom</p>
```

### Font Size

Using HTML headings, `h1` through `h6`, to size text can give you unwanted `.margin-top` and `.margin-bottom`. 
To avoid these margins but keep the `font-size`, you can instead use `.text-size-h*`. 

<div class="example">
  <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
  <p class="text-size-h1">Heading 1 Text Size</p>
  <hr>
  <p class="text-size-h2">Heading 2 Text Size</p>
  <hr>
  <p class="text-size-h3">Heading 3 Text Size</p>
  <hr>
  <p class="text-size-h4">Heading 4 Text Size</p>
  <hr>
  <p class="text-size-h5">Heading 5 Text Size</p>
  <hr>
  <p class="text-size-h6">Heading 6 Text Size</p>
</div> 

```html
<p class="text-size-h1">Heading 1 Text Size</p>
<p class="text-size-h2">Heading 2 Text Size</p>
<p class="text-size-h3">Heading 3 Text Size</p>
<p class="text-size-h4">Heading 4 Text Size</p>
<p class="text-size-h5">Heading 5 Text Size</p>
<p class="text-size-h6">Heading 6 Text Size</p>
```

### Body Copy

UA Bootstrap&apos;s global default `font-size` is **16px**, with a `line-height`
of **1.5**. This is applied to the `<body>` and all paragraphs. In addition,
`<p>` (paragraphs) receive a bottom margin of 16px by default.


<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
Nullam quis risus eget urna mollis ornare vel eu leo. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula.

Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec ullamcorper nulla non metus auctor fringilla. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Donec ullamcorper nulla non metus auctor fringilla.

Maecenas sed diam eget risus varius blandit sit amet non magna. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
</div>

```html
<p>...</p>
```


### Lead Body Copy

Make a paragraph stand out by adding `.lead`.
<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
<p class="lead">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor
auctor. Duis mollis, est non commodo luctus.</p>
</div>

```html
<p class="lead">...</p>
```


### Inline Text Elements

#### Marked Text

For highlighting a run of text due to its relevance in another context, use the
`<mark>` tag.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 You can use the mark tag to <mark>highlight</mark> text.
</div>

```html
<mark>highlight</mark>
```

#### Deleted Text

For indicating blocks of text that have been deleted use the `<del>` tag.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <del>This line of text is meant to be treated as deleted text.</del>
</div>


```html
<del>This line of text is meant to be treated as deleted text.</del>
```

#### Strikethrough Text

For indicating blocks of text that are no longer relevant use the `<s>` tag.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <s>This line of text is meant to be treated as no longer accurate.</s>
</div>

```html
<s>This line of text is meant to be treated as no longer accurate.</s>
```

#### Inserted Text

For indicating additions to the document use the `ins` tag.
<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <ins>This line of text is meant to be treated as an addition to the
document.</ins>
</div>

```html
<ins>This line of text will render as underlined.</ins>
```

#### Underlined Text

To underline text use the `u` tag.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <u>This line of text will render as underlined.</u>
</div>

```html
<u>This line of text will render as underlined.</u>
```

#### Small Text

For de-emphasizing inline or blocks of text, use the `small` tag to set text at
85% the size of the parent. Heading elements receive their own `font-size` for
nested `<small>` elements.

You may alternatively use an inline element with `.small` in place of any
`<small>`.
<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <small>This line of text is meant to be treated as fine print.</small>
</div>

```html
<small> This line of text is meant to be treated as fine print.</small>
```

#### Bold

For emphasizing a snippet of text with a heavier `font-weight`.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <p>The following snippet of text is <bold>rendered as bold text</bold>.</p>
</div>

```html
<strong>rendered as bold text</strong>
```

#### Italic

For emphasizing a snippet of text with italics.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
The following snippet of text is <em>rendered as italicized text</em>
</div>

```html
<em>rendered as italicized</em>
```


### Alignment Classes

Easily realign text to components with text alignment classes.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <p class="text-left">Left aligned text.</p>
 <p class="text-center">Center aligned text.</p>
 <p class="text-right">Right aligned text.</p>
 <p class="text-justify">Justified text.</p>
 <p class="text-nowrap">No wrap text.</p>
</div>

```html
<p class="text-left">Left aligned text.</p>
<p class="text-center">Center aligned text.</p>
<p class="text-right">Right aligned text.</p>
<p class="text-justify">Justified text.</p>
<p class="text-nowrap">No wrap text.</p>
```


### Responsive Alignment Classes


Changing alignment of text at different breakpoints allows for more design
options.

<div class="table-responsive">
    <table class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Class Name</th>
                <th>Extra small devices<small><br>Phones (&lt;48em)<br/>`text-align`</small></th>
                <th>Small devices<small><br>Tablets (≥48em)<br/>`text-align`</small></th>
                <th>Medium devices<small><br>Desktops (≥60em)<br/>`text-align`</small></th>
                <th>Large devices<small><br>Desktops (≥75em)<br/>`text-align`</small></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-not-xs`</th>
                <td>`inherit`</td>
                <td>`left`</td>
                <td>`left`</td>
                <td>`left`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-not-xs`</th>
                <td>`inherit`</td>
                <td>`center`</td>
                <td>`center`</td>
                <td>`center`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-not-xs`</th>
                <td>`inherit`</td>
                <td>`right`</td>
                <td>`right`</td>
                <td>`right`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-not-xs`</th>
                <td>`inherit`</td>
                <td>`justify`</td>
                <td>`justify`</td>
                <td>`justify`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-xs`</th>
                <td>`left`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-xs`</th>
                <td>`center`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-xs`</th>
                <td>`right`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-xs`</th>
                <td>`justify`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-not-sm`</th>
                <td>`left`</td>
                <td>`inherit`</td>
                <td>`left`</td>
                <td>`left`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-not-sm`</th>
                <td>`center`</td>
                <td>`inherit`</td>
                <td>`center`</td>
                <td>`center`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-not-sm`</th>
                <td>`right`</td>
                <td>`inherit`</td>
                <td>`right`</td>
                <td>`right`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-not-sm`</th>
                <td>`justify`</td>
                <td>`inherit`</td>
                <td>`justify`</td>
                <td>`justify`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-sm`</th>
                <td>`inherit`</td>
                <td>`left`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-sm`</th>
                <td>`inherit`</td>
                <td>`right`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-sm`</th>
                <td>`inherit`</td>
                <td>`justify`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-not-md`</th>
                <td>`left`</td>
                <td>`left`</td>
                <td>`inherit`</td>
                <td>`left`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-not-md`</th>
                <td>`center`</td>
                <td>`center`</td>
                <td>`inherit`</td>
                <td>`center`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-not-md`</th>
                <td>`right`</td>
                <td>`right`</td>
                <td>`inherit`</td>
                <td>`right`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-not-md`</th>
                <td>`justify`</td>
                <td>`justify`</td>
                <td>`inherit`</td>
                <td>`justify`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-md`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`left`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-md`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`center`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-md`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`right`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-md`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`justify`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-not-lg`</th>
                <td>`left`</td>
                <td>`left`</td>
                <td>`left`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-not-lg`</th>
                <td>`center`</td>
                <td>`center`</td>
                <td>`center`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-right-not-lg`</th>
                <td>`right`</td>
                <td>`right`</td>
                <td>`right`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-justify-not-lg`</th>
                <td>`justify`</td>
                <td>`justify`</td>
                <td>`justify`</td>
                <td>`inherit`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-left-lg`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`left`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-center-lg`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`center`</td>
            </tr>
            <tr>
                <th scope="row" class="text-nowrap">`.text-jusitify-lg`</th>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`inherit`</td>
                <td>`justify`</td>
            </tr>
        </tbody>
    </table>
</div>


### Transformation Classes

Transform text in components with text capitalization classes.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <p class="text-lowercase">Lowercased text.</p>
 <p class="text-uppercase">Uppercased text.</p>
 <p class="text-capitalize">Capitalized text.</p>
</div>

```html
<p class="text-lowercase">Lowercased text.</p>
<p class="text-uppercase">Uppercased text.</p>
<p class="text-capitalize">Capitalized text.</p>
```


### Lists

#### Unordered

A list of items in which the order does __not__ explicitly matter.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <ul>
  <li>Lorem ipsum dolor sit amet</li>
  <li>Consectetur adipiscing elit</li>
  <li>Integer molestie lorem at massa</li>
  <li>Facilisis in pretium nisl aliquet</li>
  <li>Nulla volutpat aliquam velit</li>
  <ul>
  <li>Phasellus iaculis neque</li>
  <li>Purus sodales ultricies</li>
  <li>Vestibulum laoreet porttitor sem</li>
  <li>Ac tristique libero volutpat at</li>
  </ul>
  <li>Faucibus porta lacus fringilla vel</li>
  <li>Aenean sit amet erat nunc</li>
  <li>Eget porttitor lorem</li>
  </ul>
</div>

```html
<ul>
    <li>...</li>
</ul>
```

#### Triangle

Same as unordered list but uses blue triangles as bullets.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <ul class="triangle">
   <li> Lorem ipsum dolor sit amet</li>
   <li> Consectetur adipiscing elit</li>
   <li> Integer molestie lorem atmassa </li>
   <li> Facilisis in pretium nislaliquet </li>
   <li> Nulla volutpat aliquam velit</li>
     <ul>
       <li> Phasellus iaculis neque</li>
       <li> Purus sodales ultricies</li>
       <li> Vestibulum laoreet porttitor sem</li>
       <li> Ac tristique libero volutpat at</li>
     </ul>
   <li>Faucibus porta lacus fringilla vel</li>
   <li>Aenean sit amet erat nunc</li>
   <li>Eget porttitor lorem</li>
 </ul>
</div>

```html
<ul class="triangle">
    <li>...</li>
</ul>
```

#### Ordered

A list of items in which the order __does__ explicitly matter.

<div class="example">
 <p class="example-label text-size-h3"><span class="label label-info">Example</span></p>
 <ol>
  <li> Lorem ipsum dolor sit amet</li>
  <li> Consectetur adipiscing elit</li>
  <li> Integer molestie lorem at massa</li>
  <li> Facilisis in pretium nisl aliquet</li>
  <li> Nulla volutpat aliquam velit</li>
  <li> Faucibus porta lacus fringilla vel</li>
  <li> Aenean sit amet erat nunc</li>
  <li> Eget porttitor lorem</li>
 </ol>
</div>

```html
<ol>
    <li>...</li>
</ol>
```
