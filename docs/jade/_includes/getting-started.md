## Getting Started

<p class="lead">UA Bootstrap is the University of Arizona's flavor of
[Bootstrap](http://getbootstrap.com) (aka "Twitter Bootstrap"), a popular HTML,
CSS, and JS framework for developing responsive, mobile first projects on the
web.</a>

The UA Bootstrap CSS code is hosted from a central University of Arizona
repository.  Source code can be found on
[BitBucket](https://bitbucket.org/uadigital/ua-bootstrap). Any issues or
questions about the University of Arizona flavor of Bootstrap can be submitted
through [BitBucket](https://bitbucket.org/uadigital) or by emailing
<brand@email.arizona.edu>.

Before using UA Bootstrap, please complete the [font license agreement
form](https://brand.arizona.edu/font-license-agreement). <span class="label
label-warning">Important</span>

### UA Bootstrap Reference Links

Use these reference links to start your project. Pay attention to the appended
version name ("version"). When new versions are released, accompanying
documentation for new or updated items will include a <span class="label
label-default">New</span> or <span class="label label-info">Updated</span>
label.

```html
<!-- Normalize CSS -->
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/normalize/3.0.3/normalize.min.css">

<!-- Latest icon font stylesheet-->
<link rel="stylesheet" href="//brand.arizona.edu/sites/all/themes/brand/ua-brand-icons/ua-brand-icons.css">

<!-- Latest compiled and minified CSS icon font not included-->
<link rel="stylesheet" href="//bitbucket.org/uadigital/ua-bootstrap/downloads/ua-bootstrap-1.0.0-alpha6.min.css">

<!-- jQuery -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

```

### HTML5 doctype

UA Bootstrap utilizes certain HTML elements and CSS properties that require the
use of the HTML5 doctype. Include it at the beginning of all of your projects.

```html
<!DOCTYPE html>
<html lang="en">
...
</html>
```

### Mobile First

You can disable zooming capabilities on mobile devices by adding
`user-scalable=no` to the viewport meta tag. This disables zooming—meaning users
are only able to scroll—and results in your site feeling a bit more like
a native application. Overall, we don't recommend this on every site, so use
caution!

```html
<meta name="viewport" content="width=device-width, initial-scale=1,
maximum-scale=1, user-scalable=no">
```

To ensure proper rendering and touch zooming, add the viewport meta tag to your
`<head>`

```html
<meta name="viewport" content="width=device-width, initial-scale=1,
maximum-scale=1, user-scalable=yes">
```

### Basic Template

Start with this basic HTML template, which has been customized for the UA brand.
Copy the HTML below to begin working with a minimal Bootstrap document.

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>
    <!-- UA Brand Icons, Usually requires https to load it. Best to use //brand.arizona.edu and reference from FQDN -->
    <link href="https://brand.arizona.edu/sites/all/themes/brand/ua-brand-icons/ua-brand-icons.css" rel="stylesheet">
    <!-- UA Bootstrap -->
    <link href="http://bitbucket.org/uadigital/ua-bootstrap/downloads/ua-bootstrap-1.0.0-alpha6.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js does not work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>Hello, world!</h1>
    <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

  </body>
</html>
```
### Sticky Footer Template

The following template adds a technique to push the footer to the bottom of the
page without having to set a footer height.
This solves for the issue depicted in the image below using
the `table-row` value for the `display` property.

To add padding around the page rows you can add `.page-row-padding-top` and `.page-row-padding-bottom`.


<img src="img/sticky-footer-basic.svg"><img src="img/sticky-footer-sticky.svg">
```html
<!DOCTYPE html>
<html lang="en" class="sticky-footer">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Bootstrap 101 Template</title>
    <!-- UA Brand Icons, Usually requires https to load it. Best to use //brand.arizona.edu and reference from FQDN -->
    <link href="https://brand.arizona.edu/sites/all/themes/brand/ua-brand-icons/ua-brand-icons.css" rel="stylesheet">
    <!-- UA Bootstrap -->
    <link href="http://bitbucket.org/uadigital/ua-bootstrap/downloads/ua-bootstrap-1.0.0-alpha6.min.css" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js does not work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <header class="page-row">
        This is a header.
    </header>
    <section id="content" class="page-row page-row-expanded">
    <h1>Hello, world!</h1>
    <!-- jQuery (necessary for Bootstrap JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    </section>
    <footer class="page-row">
        This is the footer.
    </footer>
  </body>
</html>
```
