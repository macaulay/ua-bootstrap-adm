<div id="catstrap-sidebar" class="affix hidden-print hidden-xs hidden-sm affix" data-spy='affix'  data-offset-top='303'>
    <ul class="nav bs-docs-sidenav">
    <li>
      [Getting Started](#getting-started)
      <ul class="nav">
        <li>[UA Bootstrap Reference Links](#ua-bootstrap-reference-links)</li>
        <li>[HTML5 Doctype](#html5-doctype)</li>
        <li>[Mobile First](#mobile-first)</li>
        <li>[Basic Template](#basic-template)</li>
        <li>[Sticky Footer Template<span class="label label-default">New</span>](#sticky-footer-template)</li>
      </ul>
    </li>
    <li>[Want to Contribute?](#want-to-contribute-)</li>
  </ul>
</div>

